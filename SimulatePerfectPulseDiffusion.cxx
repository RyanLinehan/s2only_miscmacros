////////////////////////////////////////////////
//                                             //
// SimulatePerfectPulseDiffusion.cxx           //
// rlinehan@stanford.edu                       //
// 10/02/2019                                  //
//                                             //
// This macro generates a sample of electron   //
// times for a pulse with a given # of         //
// electrons, and compares the "perfect"       //
// widths.                                     //
//                                             //
/////////////////////////////////////////////////

#include <iostream>
#include <cmath>
#include <cstdlib>
#include "TGraph.h"
#include "TH2F.h"
#include "TH1F.h"
#include "TRandom.h"

//Define the range of nElectrons and drift time
const int nE_max = 50;
const int nE_min = 2;
const int driftTime_bins = 100;
const double driftTime_min = 0;   //us
double driftTime_max;// us
const double detectorHeight_cm = 145.6;
double driftTime_dt;
const int nPulsesAtDT = 5000;
double Vd; //cm/us


const int pulseWidth_bins = 500;
const int pulseWidth_min = 0; //us
const int pulseWidth_max = 10; //us


//For looking as a function of field
int globalFieldSelection; //in V/cm




void fit2DPlotsForWidthAndSigma(std::map<int,TH2F*> histVect, TFile * f1, double driftTime_selected);
void overPlotGraphs(TFile * f1, std::vector<double> dtVect);
void plotDistanceBetweenTwoWidths(TFile * f1, double tUp , double tDown);
void findFiducialDriftTimeVsElectronNumber(TFile * f1, std::vector<double> driftTimes);
double computeSigma(std::vector<double> times);
TGraph * ReadDigitizedPlots(std::string fileName,double scaleFactor);

void SimulatePerfectPulseDiffusion(int field_VpCm)
{

  globalFieldSelection = field_VpCm;  


  //First, read in the drift velocity and the DL as a function of field. Units are cm and us
  TGraph * g_velVsField = ReadDigitizedPlots("/Users/ryanlinehan/LZ_Local_Work/LZAna/S2Only/GitlabRepo/DigitizedPlots/HogenbirkDrift2018.csv",0.1);
  TGraph * g_DLVsField = ReadDigitizedPlots("/Users/ryanlinehan/LZ_Local_Work/LZAna/S2Only/GitlabRepo/DigitizedPlots/HogenbirkDL2018.csv",1.0e-6);
  
  //Evaluate the vel and DT at the desired field, and then compute drift times and stuff from this
  Vd = g_velVsField->Eval((double)globalFieldSelection);
  double Dl = g_DLVsField->Eval((double)globalFieldSelection);  
  driftTime_max = detectorHeight_cm/Vd;
  driftTime_dt = (driftTime_max-driftTime_min)/driftTime_bins;


  std::cout << "Vd: " << Vd << ", Dl: " << Dl << ", driftTime_max: " << driftTime_max << std::endl;


  //Pseudocode
  //Plots I want:
  //1. For a given N electrons, I want a drift time vs. pulse width histogram.
  //2. Various projections of ^ for various different N values.
  TRandom randomRoot;
  randomRoot.SetSeed(0);

  //Root output
  TFile * f1 = new TFile("PerfectPulseDiffusionOutput.root","RECREATE");



  //Define the drift velocity and diffusion constant
  //  double Vd = 0.17; //Want cm/us
  //double Dl = 1.6e-5; //want cm^2/us

  double seWidth = 1.0;//1.5;

  //Array of persistent histos
  std::map<int,TH2F*> map_nE_pulseTimeVsWidthVect;

  //Loop over drift distances

  /////////////////////////////////////////////
  // 2D HISTO CREATION
  //-------------------------------------------

  //Loop over Nelectron values
  for( int iN = nE_min; iN <= nE_max; ++iN ){
    int numElectrons = iN;

    std::cout << "Simulating #Electrons=" << numElectrons << std::endl;

    char name[200]; 
    sprintf(name,"wdthVsDt_%de-",numElectrons);
    char title[200];
    sprintf(title,"Perfect Pulse Width vs. Drift Time, %d e- in Pulse; Drift Time [us]; Pulse Width [us]",numElectrons);
    TH2F* h1 = new TH2F(name,title,driftTime_bins,driftTime_min,driftTime_max,pulseWidth_bins,pulseWidth_min,pulseWidth_max);
    

    //Loop over drift times    
    for( int iT = 0; iT < driftTime_bins; ++iT ){
      double driftTime = iT*driftTime_dt;

      //Draw this many pulses at this drift time
      for( int iP = 0; iP < nPulsesAtDT; ++iP ){

	//Create a vector to hold the electron "start" times, in us relative to pulse center.
	std::vector<double> eStartTimes;
	for( int in = 0; in < numElectrons; in++ ){
	  
	  //Define a sigma for our probability distribution
	  double sigma = TMath::Power(2*Dl*driftTime,0.5)/Vd;
	  
	  //Draw from a probability distribution
	  double time = randomRoot.Gaus(0,sigma);
	  eStartTimes.push_back(time);

	}
	
	//Sort the vector according to start times and identify the last minus first time
	//Call this the width of the pulse
	std::sort(eStartTimes.begin(),eStartTimes.end());

	//Compute the sigma of the times
	double sigma = computeSigma(eStartTimes);
	double fullPulseSigma = TMath::Power(sigma*sigma + seWidth*seWidth/12,0.5);


	//Find a different metric: width
	double width = eStartTimes[eStartTimes.size()-1] - eStartTimes[0] + seWidth;
	h1->Fill(driftTime,sigma);
	
      }
    }

    //push the histogram back
    map_nE_pulseTimeVsWidthVect.emplace(numElectrons,h1);

  }



  /*
  ///////////////////////////////////////////////
  // Misc Width/Sigma finding
  //---------------------------------------------

  //Fit the 2D plots to gaussians in width for the last drift bin
  fit2DPlotsForWidthAndSigma(map_nE_pulseTimeVsWidthVect,f1,driftTime_max-1);
  fit2DPlotsForWidthAndSigma(map_nE_pulseTimeVsWidthVect,f1,driftTime_max/2.);
  fit2DPlotsForWidthAndSigma(map_nE_pulseTimeVsWidthVect,f1,driftTime_max/4.);
  
  //Plot the distance between adjacent graphs
  plotDistanceBetweenTwoWidths(f1,driftTime_max-1,driftTime_max/4.);
  */











  ////////////////////////////////////////////////
  // FIDUCIAL DRIFT TIME VS ELECTRON NUMBER
  //----------------------------------------------

  //Scan through and make a bunch of plots of width and sigma for various drift times
  int nScanBins = 50;
  std::vector<double> driftTimes;
  for( int iT = 1; iT < nScanBins; ++iT ){
    double driftTime_iT = driftTime_max/((double)nScanBins)*iT;
    fit2DPlotsForWidthAndSigma(map_nE_pulseTimeVsWidthVect,f1,driftTime_iT);
    driftTimes.push_back(driftTime_iT);
  }
  fit2DPlotsForWidthAndSigma(map_nE_pulseTimeVsWidthVect,f1,driftTime_max-1);
  driftTimes.push_back(driftTime_max-1);

  //Now find for a given electron number, what the fiducial dift time is (i.e. where the "cathode free" area is)
  findFiducialDriftTimeVsElectronNumber(f1, driftTimes);



  /*  
  ///////////////////////////////////////////////////
  // MISC PLOTTING
  //-------------------------------------------------
   
  //Plot multiple of these against each other
  std::vector<double> timeVect;
  timeVect.push_back(driftTime_max-1);
  timeVect.push_back(driftTime_max/2.);
  timeVect.push_back(driftTime_max/4.);
  overPlotGraphs(f1,timeVect);
  */    
  

  
  /*  
  //Now go print
  for( std::map<int,TH2F*>::iterator it = map_nE_pulseTimeVsWidthVect.begin(); it != map_nE_pulseTimeVsWidthVect.end(); ++it ){
    it->second->Write();
  }
  */
  f1->Close();
    
}


void fit2DPlotsForWidthAndSigma(std::map<int,TH2F*> histVect, TFile * f1, double driftTime_selected)
{
  //Persistency
  TGraphErrors * g1 = new TGraphErrors(histVect.size());
  char name[100];
  sprintf(name,"MeanSigmaDriftWidth_%dus",((int)driftTime_selected));
  g1->SetName(name);
  char title[200];
  sprintf(title,"Mean and sigma of drift width, for drift time of %d us",((int)driftTime_selected));
  g1->SetTitle(title);
  g1->GetXaxis()->SetTitle("Number of e- in pulse");
  g1->GetYaxis()->SetTitle("Pulse Width (us)");


  //Second graph: the ratio of the mean width to the uncertainty/spread
  TGraph * g2 = new TGraph(histVect.size());
  sprintf(name,"MeanWidthDivBySigma_%dus",((int)driftTime_selected));
  g2->SetName(name);
  sprintf(title,"Mean (additional) width divided by uncertainty in width, for drift time of %d us",((int)driftTime_selected));
  g2->SetTitle(title);
  g2->GetXaxis()->SetTitle("Number of e- in pulse");
  g2->GetYaxis()->SetTitle("Width/Uncertainty");
  
  
  //Loop over hist vect and create a projection histogram
  int counter = 0;
  for( std::map<int,TH2F*>::iterator it = histVect.begin(); it != histVect.end(); ++it ){
    
    int selectedBinX = it->second->GetXaxis()->FindBin(driftTime_selected);
    std::cout << "Selected bin: " << selectedBinX << std::endl;
    char name[100];
    sprintf(name,"ProjY_%de-_%dus",it->first,((int)driftTime_selected));
    TH1F * projY = (TH1F*)it->second->ProjectionY(name,selectedBinX,selectedBinX);
    TF1 * f1 = new TF1("g","gaus",pulseWidth_min,pulseWidth_max);
    f1->SetParameter(1,(pulseWidth_max-pulseWidth_min)/2);
    f1->SetParameter(2,2.0);
    projY->Fit(f1);
    projY->Write();
    
    //Output the sigma and 
    g1->SetPoint(counter,it->first,f1->GetParameter(1));
    g1->SetPointError(counter,0,f1->GetParameter(2));
    g2->SetPoint(counter,it->first,f1->GetParameter(1)/f1->GetParameter(2));
    counter++;
  }

  g1->Write();
  g2->Write();
}


//Plot some of the computed graphs on the same axes
void overPlotGraphs(TFile * f1, std::vector<double> dtVect)
{
  TCanvas * c1 = new TCanvas();
  TLegend * l1 = new TLegend(0.65,0.15,0.85,0.35);
  std::vector<TGraphErrors*> graphVect;
  for( int iT = 0; iT < dtVect.size(); ++iT ){
    char name[100];
    sprintf(name,"MeanSigmaDriftWidth_%dus",((int)dtVect[iT]));
    TGraphErrors * g1 = (TGraphErrors*)f1->Get(name);
    graphVect.push_back(g1);
    g1->SetMarkerStyle(21);
    g1->SetMarkerColor(30+4*iT);
    g1->SetLineColor(30+4*iT);
    if( iT == 0 ){
      g1->Draw("AP");
    }
    else{
      g1->Draw("Psame");
    }
    sprintf(name,"Drift=%dus",((int)dtVect[iT]));
    l1->AddEntry(g1,name);
  }
  l1->Draw("same");
  
}

//Plot the distance between two of the drift distances
void plotDistanceBetweenTwoWidths(TFile * f1, double tUp , double tDown)
{
  TCanvas * c1 = new TCanvas();
  char name[100];
  sprintf(name,"MeanSigmaDriftWidth_%dus",((int)tUp));
  TGraphErrors * gUp = (TGraphErrors*)f1->Get(name);
  sprintf(name,"MeanSigmaDriftWidth_%dus",((int)tDown));
  TGraphErrors * gDown = (TGraphErrors*)f1->Get(name);
  
  //Make a new TGraph for the distance
  TGraphErrors * gSub = new TGraphErrors(gUp->GetN());
  

  //Loop over the points in gUp
  for( int iP = 0; iP < gUp->GetN(); ++iP ){
    double xU,yU,xD,yD;
    gUp->GetPoint(iP,xU,yU);
    gDown->GetPoint(iP,xD,yD);
    double eyU = gUp->GetErrorY(iP);
    double eyD = gDown->GetErrorY(iP);
    if( xU != xD ) std::cout << "Uh oh. Problem here!!!!" << std::endl;
    gSub->SetPoint(iP,xD,((yU-yD)/eyU));
  }
  char title[200];
  sprintf(title,"Distance between %d us and %d us means, divided by %d us sigma",((int)tUp),((int)tDown),((int)tUp));
  gSub->SetTitle(title);
  gSub->GetXaxis()->SetTitle("Number of e- in pulse");
  gSub->GetYaxis()->SetTitle("(DeltaWidth)/(Sigma)");
  gSub->SetMarkerStyle(21);
  gSub->SetMarkerColor(30);
  gSub->Draw("AP");
}

//Loop through the electron numbers and identify what drift time gives a 2 sigma separation between
//itself and the 799 drift.
void findFiducialDriftTimeVsElectronNumber(TFile * f1, std::vector<double> driftTimes)
{
  
  //Create a tgraph for storing this information
  TGraph * gFinal = new TGraph();
  gFinal->SetName("CathodeFreeVolume");
  const int nSigmaIsDistant = 2; //How far is far enough away from the cathode (in cathode sigmas)

  //Loop over numbers of electrons
  int counter = 0;
  for( int iE = nE_min; iE < nE_max; ++iE ){

    //For this electron number, create a map of drift time to pulse width and sigma
    std::vector<std::pair<double,double> > vectWidthSigma;
    for( int iD = 0; iD < driftTimes.size(); ++iD ){
      double dT = driftTimes[iD];
      char name[100];
      sprintf(name,"MeanSigmaDriftWidth_%dus",((int)dT));
      TGraphErrors * g1 = (TGraphErrors*)f1->Get(name);
      
      //Loop through the points in the plot and find the one corresponding to this # of electrons.
      //Find the y  value and sigma at that point.
      double x, y, sig;
      for( int iP = 0; iP < g1->GetN(); ++iP ){
	double xT, yT;
	g1->GetPoint(iP,xT,yT);
	if( xT == iE ){
	  x = xT;
	  y = yT;
	  sig = g1->GetErrorY(iP);
	  break;
	}
      }
      
      std::pair<double,double> pairWidthSigma(y,sig);
      vectWidthSigma.push_back(pairWidthSigma);
    }

    //Now loop through the pairWidthSigmas and find the last one that is sufficiently "distant" from the 
    //cathode, in cathode sigmas.
    double fidVolBoundTime = 0;
    for( int iD = 0; iD < driftTimes.size(); ++iD ){
      double width = vectWidthSigma[iD].first;
      double lastWidth = vectWidthSigma[driftTimes.size()-1].first;
      double lastSigma = vectWidthSigma[driftTimes.size()-1].second;
      double distance = (lastWidth-width)/lastSigma;
      std::cout << "iD: " << iD << ", Width: " << width << ", lastWidth: " << lastWidth << ", sigma: " << lastSigma << std::endl;
      if( distance > nSigmaIsDistant ) fidVolBoundTime = driftTimes[iD];
    }

    std::cout << "For " << iE << " electrons, fidVolBoundTime: " << fidVolBoundTime << std::endl;
   
    gFinal->SetPoint(counter,iE,fidVolBoundTime*Vd); //Should normalize by the drift velocity to get the distance from the liquid surface.
    counter++;
  }

  //Print this plot
  TCanvas * c1 = new TCanvas();
  gFinal->SetMarkerStyle(21);
  gFinal->SetMarkerColor(35);
  gFinal->SetLineColor(33);
  gFinal->GetXaxis()->SetTitle("Number of e- in pulse");
  gFinal->GetYaxis()->SetTitle("Fiducial Z Bound [cm]");
  char title[200];
  sprintf(title,"Fiducial Z Bound (at %d sigma from Cathode)",nSigmaIsDistant);
  gFinal->SetTitle(title);
  gFinal->Draw("ALP");

  char fileTitle[100];
  sprintf(fileTitle,"CathodeFreeVolume_field_%d.root",globalFieldSelection);
  TFile * f2 = new TFile(fileTitle,"RECREATE");
  gFinal->Write();
  f2->Close();
  
  
}


double computeSigma(std::vector<double> times)
{
  //Loop over the vector
  double mean;
  double mean2;
  for( int iT = 0; iT < times.size(); ++iT ){
    mean += times[iT];
    mean2 += times[iT]*times[iT];
  }
  mean/=times.size();
  mean2/=times.size();
  double sigma2 = mean2-mean*mean;
  sigma2 = pow(sigma2,0.5);
  return sigma2;
}


TGraph * ReadDigitizedPlots(std::string fileName,double scaleFactor)
{
  std::ifstream infile;
  infile.open(fileName.c_str());
  double x,y;
  TGraph * output = new TGraph();
  int counter = 0;
  while(1){
    infile >> x >> y;
    if( !infile.good() )break;
    output->SetPoint(counter,x,y*scaleFactor);
    counter++;
  }
  return output;
}
