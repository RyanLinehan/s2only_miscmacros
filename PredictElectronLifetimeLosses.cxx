////////////////////////////////////////////
//
//  PredictElectronLifetimeLosses.cxx
//
//  rlinehan@stanford.edu
//  9/9/2020
//
//  This code is intended to see how a
//  changing drift velocity impacts 
//  the electron losses due to a finite
//  electron lifetime.
//
////////////////////////////////////////////

#include <iostream>
#include <cstdlib>

TGraph * ReadDigitizedPlots(std::string fileName,double scaleFactor);

void PredictElectronLifetimeLosses()
{
  TRandom * trnd = new TRandom(0);


  TGraph * g_velVsField = ReadDigitizedPlots("/Users/ryanlinehan/LZ_Local_Work/LZAna/S2Only/GitlabRepo/DigitizedPlots/HogenbirkDrift2018.csv",0.1);
  const double detectorHalfHeight = 145.6/2.0;
  const double electronLifetime = 880; //us
  const int nF = 4;
  const double minF = 30;
  const double dF = 50;
  std::map<double,TH1F*> map_field_nEDistribution;
  for( int iF = 0; iF < nF; ++iF ){
    double field;
    if( iF == 0 ) field = minF;
    if( iF == 1 ) field = 60;
    if( iF == 2 ) field = 100;
    if( iF == 3 ) field = 300;
    double vel = g_velVsField->Eval(field);
    double time_us = detectorHalfHeight/vel;
    double driftSurvivalProb = TMath::Exp(-time_us/electronLifetime);
    double extractionProb = 0.85;
    char name1[40];
    sprintf(name1,"survivingPulseSize_f%d",(int)field);
    TH1F * h1 = new TH1F(name1,"Size of S2 for 10e- Pulse",25,0,25);
    map_field_nEDistribution.emplace(field,h1);
    std::cout << "For field: " << field << ", vel: " << vel << ", survival probability is: " << driftSurvivalProb << std::endl;

    //Loop over pulses
    const int nP = 1000;
    for( int iP = 0; iP < nP; ++iP ){
      
      //Loop over electrons
      int nElectrons = 20;
      int survivingElectrons = 0;
      for( int iE = 0; iE < nElectrons; ++iE ){
	if( trnd->Uniform() < driftSurvivalProb ){
	  if( trnd->Uniform() < extractionProb ){
	    survivingElectrons++;
	  }
	}
      }
      map_field_nEDistribution[field]->Fill(survivingElectrons);
    }
    map_field_nEDistribution[field]->GetXaxis()->SetTitle("NElectrons Surviving in 20e- Bunch");
    map_field_nEDistribution[field]->GetYaxis()->SetTitle("Counts");
  }
  
  //Now plot
  TCanvas * c1 = new TCanvas();
  TLegend * l1 = new TLegend();
  int ctr = 202;
  for( std::map<double,TH1F*>::iterator it = map_field_nEDistribution.begin(); it != map_field_nEDistribution.end(); ++it ){
    char name[40];
    sprintf(name,"Field=%d V/cm",(int)it->first);
    l1->AddEntry(it->second,name);
    it->second->SetLineColor(ctr);
    it->second->SetLineWidth(2);
    if( ctr == 202 ){
      it->second->Draw();
    }
    else{
      it->second->Draw("same");
    }
    ctr+=4;
  }
  l1->Draw("same");
  
  
  
}


TGraph * ReadDigitizedPlots(std::string fileName,double scaleFactor)
{
  std::ifstream infile;
  infile.open(fileName.c_str());
  double x,y;
  TGraph * output = new TGraph();
  int counter = 0;
  while(1){
    infile >> x >> y;
    if( !infile.good() )break;
    output->SetPoint(counter,x,y*scaleFactor);
    counter++;
  }
  return output;
}
