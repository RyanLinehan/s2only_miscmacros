///////////////////////////////////////////////
//
//  SimpleDiffusionPlotsVsField.cxx
//
//  rlinehan@stanford.edu
//  9/6/2020
//
//  This script produces plots that convolve
//  the diffusion and drift velocity to show
//  the effect of drift field on the spread
//  of a given S2 pulse. Not a monte carlo.
//  Just equations.
//
///////////////////////////////////////////////

#include <iostream>
#include <cstdlib>

TGraph * ReadDigitizedPlots(std::string fileName,double scaleFactor);


void SimpleDiffusionPlotsVsField()
{
  TGraph * g_velVsField = ReadDigitizedPlots("/Users/ryanlinehan/LZ_Local_Work/LZAna/S2Only/HogenbirkDrift2018.csv",0.1);
  TGraph * g_DLVsField = ReadDigitizedPlots("/Users/ryanlinehan/LZ_Local_Work/LZAna/S2Only/HogenbirkDL2018.csv",1.0e-6);
  TGraph * g_DTVsField = ReadDigitizedPlots("/Users/ryanlinehan/LZ_Local_Work/LZAna/S2Only/EXO200.csv",1.0e-6);

  TCanvas * c0_1 = new TCanvas();
  g_velVsField->Draw("ALP");
  
  
  //Set up the graphs for the different heights in the detector
  std::map<int,TGraph*> map_height_graphLongDiff;
  std::map<int,TGraph*> map_height_graphLatDiff;
  const int nHeights = 6;
  const double FFRHeight = 145.0; //cm
  for( int iH = 0; iH < nHeights; ++iH ){
    TGraph * g1 = new TGraph();
    TGraph * g2 = new TGraph();
    double dH = FFRHeight/((double)nHeights);
    map_height_graphLongDiff.emplace(iH*dH,g1);
    map_height_graphLatDiff.emplace(iH*dH,g2);
  }


  //Larger loop: loop over fields
  const int nFields = 40;
  const double dF = 10; //V/cm
  const double minF = 30; //V/cm
  const double sigma0 = 1.000/TMath::Power(12.,0.5); //us
  for( int iF = 0; iF < nFields; ++iF ){
    double field = minF + dF*iF;
    
    //Evaluate the transport parameters at a given field
    double vel = g_velVsField->Eval(field);
    double DL = g_DLVsField->Eval(field);
    double DT = g_DTVsField->Eval(field);
    
    std::cout << "Field: " << field << ", vel: " << vel << ", DL: " << DL << ", DT: " << DT << std::endl;

    //Smaller loop: loop over heights
    for( std::map<int,TGraph*>::iterator it = map_height_graphLongDiff.begin(); it != map_height_graphLongDiff.end(); ++it ){
      double height = it->first;
      double driftTime_us = ((FFRHeight+0.6)-height)/vel;
      double diffusionWidth_Long = TMath::Power(2*DL*driftTime_us/vel/vel + sigma0*sigma0,0.5);
      double diffusionWidth_Lat = TMath::Power(2*DT*driftTime_us,0.5);
      map_height_graphLongDiff[height]->SetPoint(iF,field,diffusionWidth_Long);
      map_height_graphLatDiff[height]->SetPoint(iF,field,diffusionWidth_Lat);
    }
  }
  

  //Now draw the graphs


  //Longitudinal diffusion
  TCanvas * c1 = new TCanvas();
  TLegend * l1 = new TLegend();
  int counterColor = 202;
  for( std::map<int,TGraph*>::iterator it = map_height_graphLongDiff.begin(); it != map_height_graphLongDiff.end(); ++it ){  
    it->second->SetLineColor(counterColor);
    it->second->SetLineWidth(5);
    it->second->SetMarkerColor(counterColor);
    it->second->SetMarkerStyle(21);
    char name[40];
    sprintf(name,"Height: %0.1d cm Above Cathode",it->first);
    l1->AddEntry(it->second,name);
    if( counterColor == 202 ){ 
      it->second->SetTitle("Longitudinal Diffusion vs. FFR Field");
      it->second->GetXaxis()->SetTitle("Field (V/cm)");
      it->second->GetYaxis()->SetTitle("Pulse Sigma (us)");
      it->second->Draw("ALP");
    }
    else{ it->second->Draw("LPsame"); }
    counterColor+=4;
  }
  l1->Draw("same");
  


  //Lateral diffusion
  TCanvas * c2 = new TCanvas();
  TLegend * l2 = new TLegend();
  counterColor = 202;
  for( std::map<int,TGraph*>::iterator it = map_height_graphLatDiff.begin(); it != map_height_graphLatDiff.end(); ++it ){  
    it->second->SetLineColor(counterColor);
    it->second->SetLineWidth(5);
    it->second->SetMarkerColor(counterColor);
    it->second->SetMarkerStyle(21);
    char name[40];
    sprintf(name,"Height: %0.1d cm Above Cathode",it->first);
    l2->AddEntry(it->second,name);
    if( counterColor == 202 ){
      it->second->SetTitle("Lateral Diffusion vs. FFR Field");
      it->second->GetXaxis()->SetTitle("Field (V/cm)");
      it->second->GetYaxis()->SetTitle("Electron Bunch Sigma (cm)");
      it->second->Draw("ALP");
    }
    else{ it->second->Draw("LPsame"); }
    counterColor+=4;
  }
  l2->Draw("same");
}





TGraph * ReadDigitizedPlots(std::string fileName,double scaleFactor)
{
  std::ifstream infile;
  infile.open(fileName.c_str());
  double x,y;
  TGraph * output = new TGraph();
  int counter = 0;
  while(1){
    infile >> x >> y;
    if( !infile.good() )break;
    output->SetPoint(counter,x,y*scaleFactor);
    counter++;
  }
  return output;
}
